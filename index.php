<?php

//require('Animal.php');
require_once('Frog.php');
require_once('Ape.php');
$object = new Animal("Shaun");
echo "Nama Hewan : $object->name <br>";
echo "Legs :  $object->legs <br>";
echo "Cold Blooded:  $object->Cold_Blooded <br> <br>";
 
$object2 = new Frog("Buduk");
echo "Name : $object2->name <br>";
echo "Legs: $object2->legs <br>";
echo "Cold_Blooded: $object2->Cold_Blooded <br>";
echo "Jump : $object2->Jump  <br> <br>";

$object3 = new Ape("Kera Sakti");
echo "Name : $object3->name <br>";
echo "Legs: $object3->legs <br>";
echo "Cold_Blooded: $object3->Cold_Blooded <br>";
echo "Yell : $object3->Yell  <br>";

